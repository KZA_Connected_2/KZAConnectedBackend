# KZAConnected 2

## Configuratie
1. Open de docker-compose.yml. Kies een gebruikersnaam en (root-) wachtwoord.
2. Kopieer de application.properties.example (src/main/resources) en rename naar application.properties
3. Gebruik als gebruiker en wachtwoord dezelfde als in (1)
4. Bouw je applicatie dmv 'mvn package'
5. Check of de jar in de target folder qua naam matcht met de 2 referenties naar deze jar in de Dockerfile
6. Maak de image en draai de containers dmv 'docker-compose up'
7. De app luistert naar http://192.168.99.100:8888/api/cursussen

## TODO Cursussen-service
- SWAGGER yaml file maken als API/interface definitie
- Deze yaml gebruiken voor het aanmaken en onderhouden van de API
- Iets met omgevingsvariabelen
- Iets met testen mbv de spring boot memory db: hsqldb en RestAssured
- Etc.

## TODO
- Authenticatie/Autorisatie-service?
- Gebruikers-service
- Kennisgebieden-service
- Klanten-service
- Competenties-service
- Etc.