FROM openjdk:8-jdk-alpine
VOLUME /tmp
EXPOSE 8080
ADD /target/cursus-service-0.0.1-SNAPSHOT.jar cursus-service-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","cursus-service-0.0.1-SNAPSHOT.jar"]