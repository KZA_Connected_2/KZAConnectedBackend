package com.kzaconnected.cursusservice.Repository;

import com.kzaconnected.cursusservice.Model.Cursus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CursusRepository extends JpaRepository<Cursus,Long> {
}
