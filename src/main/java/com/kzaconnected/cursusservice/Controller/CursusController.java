package com.kzaconnected.cursusservice.Controller;

import com.kzaconnected.cursusservice.Model.Cursus;
import com.kzaconnected.cursusservice.Repository.CursusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CursusController {

    @Autowired
    private CursusRepository cursusRepository;

    @GetMapping("/cursussen/{id}")
    public ResponseEntity<Cursus> getCursusById(@PathVariable(value = "id") Long cursusId) {
        Cursus cursus = cursusRepository.findOne(cursusId);
        if(cursus == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(cursus);
    }

    @GetMapping("/cursussen")
    public List<Cursus> getAllCursussen() {
        return cursusRepository.findAll();
    }

    @PostMapping("/cursussen")
    public Cursus createCursus(@Valid @RequestBody Cursus cursus) {
        return cursusRepository.save(cursus);
    }

    @PutMapping("/cursussen/{id}")
    public ResponseEntity<Cursus> updateCursus(@PathVariable(value = "id") Long cursusId,
                                           @Valid @RequestBody Cursus cursus) {
        Cursus c = cursusRepository.findOne(cursusId);
        if(cursus == null) {
            return ResponseEntity.notFound().build();
        }
        c.setNaam(cursus.getNaam());

        Cursus updatedCursus = cursusRepository.save(cursus);
        return ResponseEntity.ok(updatedCursus);
    }

    @DeleteMapping("/cursussen/{id}")
    public ResponseEntity<Cursus> deleteCursus(@PathVariable(value = "id") Long cursusId) {
        Cursus cursus = cursusRepository.findOne(cursusId);
        if(cursus == null) {
            return ResponseEntity.notFound().build();
        }

        cursusRepository.delete(cursus);
        return ResponseEntity.ok().build();
    }
}
