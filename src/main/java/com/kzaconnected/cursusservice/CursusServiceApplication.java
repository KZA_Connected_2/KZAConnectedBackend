package com.kzaconnected.cursusservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class CursusServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CursusServiceApplication.class, args);
	}
}
