package com.kzaconnected.cursusservice.Model;

import org.hibernate.validator.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "cursussen")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"aangemaaktOp", "gewijzigdOp"}, allowGetters = true)
public class Cursus implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    private String naam;

//    @NotBlank
//    private Set<CursusDatum> cursusData;
//
//    @NotBlank
//    private Set<CursusDocent> cursusDocenten;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date aangemaaktOp;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date gewijzigdOp;

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }
}